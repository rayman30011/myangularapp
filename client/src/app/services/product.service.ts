import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/shop.models';

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) { }

  public getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>("/api/product");
  }
  public getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`/api/product/${id}`);
  }
}
