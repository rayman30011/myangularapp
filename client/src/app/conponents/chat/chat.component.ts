import { Component, OnInit } from '@angular/core';
import * as signalR from '@aspnet/signalr';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  messages: string[] = [];
  msg: string = '';
  private _hubConnection: signalR.HubConnection;

  ngOnInit(): void {
    this._hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl('http://localhost:5000/hubs/chat',{
                              accessTokenFactory: () => localStorage.getItem('token') 
                            }).build();
 
    this._hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));

    this._hubConnection.on("Receive", (message: string) => {
      this.messages.push(message);
    });
  }

  sendMessage() {
    this._hubConnection.send("Send", this.msg)
      .then(() => {
        this.messages.push(this.msg);
        this.msg = '';
      }).catch(() => this.msg = 'Error');
  }

}
