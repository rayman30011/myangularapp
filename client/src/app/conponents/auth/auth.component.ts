import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  userName = '';
  password = '';

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  auth() {
    this.http.post('/api/auth/token', {
      userName: this.userName, 
      password: this.password
    }).subscribe((token: any) => {
      localStorage.setItem('user', token.userName);
      localStorage.setItem('token', token.token);

      this.router.navigate(['/chat']);
    });
  }
}
