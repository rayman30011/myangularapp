import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProductComponent } from './componets/product/product.component';
import { ProductService } from './services/product.service';
import { MainPageComponent } from './componets/main-page/main-page.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { RouterModule } from '@angular/router';
import { AuthComponent } from './conponents/auth/auth.component';
import { ChatComponent } from './conponents/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    MainPageComponent,
    AuthComponent,
    ChatComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    ProductService,
  ],
  bootstrap: [AppComponent], 
})
export class AppModule { }
