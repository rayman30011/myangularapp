﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AuthApp.Controllers
{
    public class AuthModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;

        public AuthController(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("token")]
        public async Task<IActionResult> GetToken(AuthModel model)
        {
            if (!_userManager.Users.Any())
            {
                var result = await _userManager.CreateAsync(new IdentityUser { UserName = "user" }, "Asdf-1234");
            }

            var user = _userManager.Users.FirstOrDefault(u => u.UserName == model.UserName);
            var claims = await _userManager.GetClaimsAsync(user);
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id));

            var jwt = new JwtSecurityToken(
                issuer: "MySuperApp",
                audience: "http://localhost:6060/",
                notBefore: DateTime.UtcNow,
                claims: claims,
                expires: DateTime.UtcNow.AddSeconds(30),
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes("mysupersecret_secretkey!123")),
                    SecurityAlgorithms.HmacSha256));

            var token = new JwtSecurityTokenHandler().WriteToken(jwt);

            return Ok(new
            {
                token,
                user.UserName,
            });
        }

        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            var a = User;
            return Ok(new[] { "value 1", "value 2" });
        }
    }
}